const fastifyplugin = require('fastify-plugin');
const fastifyCors = require('@fastify/cors');

// Mas informacion: https://github.com/fastify/fastify-cors
module.exports = fastifyplugin((fastify, _, done) => {
  const corsConfig = {};
  fastify.register(fastifyCors, corsConfig);
  done();
});

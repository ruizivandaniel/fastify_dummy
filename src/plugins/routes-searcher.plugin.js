const fastifyPlugin = require('fastify-plugin');

module.exports = fastifyPlugin((fastify, _, done) => {
  const routes = [];
  fastify
    .addHook('onRoute', newroute => {
      routes.push(newroute);
    })
    .addHook('onReady', done => {
      console.log('[routes-searcher]');
      let currentTag, prevTag = '';
      routes.forEach(route => {
        currentTag = route.schema.tags[0] || '';
        if (currentTag && currentTag !== prevTag) {
          prevTag = currentTag;
          console.log(`${currentTag}`);
        }
        console.log(`\t${route.method}\t${route.url}`);
      });
      done();
    })
  done();
});

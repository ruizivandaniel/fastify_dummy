const fastifyplugin = require('fastify-plugin');
const objectIdSchema = require('./../global-schemas/objectid.schema');
const userSchema = require('./../global-schemas/user.schema');
const authorSchema = require('./../global-schemas/author.schema');
const bookSchema = require('./../global-schemas/book.schema');

module.exports = fastifyplugin((fastify, _, done) => {
  fastify
    .addSchema(objectIdSchema)
    .addSchema(userSchema)
    .addSchema(authorSchema)
    .addSchema(bookSchema);
  done();
});

const fastifyplugin = require('fastify-plugin');
const fastifyEnv = require('@fastify/env');

// Mas informacion: https://github.com/fastify/fastify-env
module.exports = fastifyplugin((fastify, _, done) => {
  const envPluginConfig = {
    data: process.env,
    dotenv: true,
    confKey: 'config',
    schema: {
      type: 'object',
      required: [
        'API_PORT', 'API_HOST', 'API_VERSION', 'DB_HOST', 'DB_PORT',
        'DB_NAME', 'BOOKS_COLL', 'AUTHORS_COLL', 'API_JWT_SECRET'],
      properties: {
        // app
        API_PROD_MODE: { type: 'boolean', default: false },
        API_HOST: { type: 'string', default: 'localhost' },
        API_VERSION: { type: 'string', default: 'v1' },
        API_PORT: { type: 'integer', default: 3000 },
        API_JWT_SECRET: { type: 'string', default: 'MyJWTSecret' },
        // database
        DB_HOST: { type: 'string', default: 'localhost' },
        DB_PORT: { type: 'integer', default: 27017 },
        DB_NAME: { type: 'string', default: 'test' },
        // collections names
        BOOKS_COLL: { type: 'string', default: 'books' },
        AUTHORS_COLL: { type: 'string', default: 'authors' },
        USERS_COLL: { type: 'string', default: 'users' },
      }
    },
  };
  fastify.register(fastifyEnv, envPluginConfig);
  done();
});

const fastifyplugin = require('fastify-plugin');
const fastifyMongo = require('@fastify/mongodb');

// Mas informacion: https://github.com/fastify/fastify-mongodb
module.exports = fastifyplugin((fastify, _, done) => {
  const mongoDbPlugin = {
    url: `mongodb://${fastify.config.DB_HOST}:${fastify.config.DB_PORT}/${fastify.config.DB_NAME}`,
    forceClose: true,
    useUnifiedTopology: true,
  };
  fastify.register(fastifyMongo, mongoDbPlugin);
  done();
});

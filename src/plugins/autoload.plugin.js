const fastifyplugin = require('fastify-plugin');
const fastifyCors = require('@fastify/autoload');
const path = require('path');

// Mas informacion: https://github.com/fastify/fastify-autoload
module.exports = fastifyplugin((fastify, opts, done) => {
  const autoloadConfig = {
    dir: path.join(opts.path, 'routes'),
    options: {
      prefix: '/api/' + fastify.config.API_VERSION,
    },
    ignorePattern: /.*(controllers|options)\.js/
  };
  fastify.register(fastifyCors, autoloadConfig);
  done();
});

const fastifyplugin = require('fastify-plugin');
const fastifyJwt = require('@fastify/jwt');

// Mas informacion: https://github.com/fastify/fastify-jwt
module.exports = fastifyplugin((fastify, _, done) => {
  const jwtConfig = {
    secret: fastify.config.API_JWT_SECRET,
  };
  fastify.register(fastifyJwt, jwtConfig);
  done();
});

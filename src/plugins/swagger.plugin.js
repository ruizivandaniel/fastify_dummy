const fastifyplugin = require('fastify-plugin');
const fastifySwagger = require('@fastify/swagger');

// Mas informacion: https://github.com/fastify/fastify-swagger
module.exports = fastifyplugin((fastify, _, done) => {
  const swaggerConfig = {
    exposeRoute: true,
    routePrefix: '/',
    // uiConfig: {
    //   docExpansion: 'full',
    //   deepLinking: false
    // },
    swagger: {
      host: fastify.config.API_HOST,
      schemes: ['http'],
      consumes: ['application/json'],
      produces: ['application/json'],
      tags: [
        { name: 'Authors', description: 'Endpoints disponibles para la api de /authors' },
        { name: 'Books', description: 'Endpoints disponibles para la api de /books' }
      ],
      info: {
        title: 'Books API',
        description: 'Swagger de Books API.',
        version: fastify.config.API_VERSION,
      },
      // mostrar informacion adicional
      // externalDocs: {
      //   url: 'https://swagger.io',
      //   description: 'Find more info here'
      // },
    }
  };
  fastify.register(fastifySwagger, swaggerConfig);
  done();
});

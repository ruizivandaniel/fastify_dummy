const corsPlugin = require('./plugins/cors.plugin');
const mongoDbPlugin = require('./plugins/mongodb.plugin');
const jwtPlugin = require('./plugins/jwt.plugin');
const swaggerPlugin = require('./plugins/swagger.plugin');
const routeSearcherPlugin = require('./plugins/routes-searcher.plugin');
const autoloadPlugin = require('./plugins/autoload.plugin');
const envPlugin = require('./plugins/env.plugin');
const globalSchemasPlugin = require('./plugins/global-schemas.plugin');

/** Fastify instance */
const fastify = require('fastify')({
  logger: { level: 'error' }, // logger: true,
});

/** Init fastify */
(async () => {
  try {
    /* Environment config */
    await fastify.register(envPlugin).after();
    /** Conexion a base */
    fastify.register(mongoDbPlugin);
    /** Configuracion de cors */
    fastify.register(corsPlugin);
    /** Configuracion de jwt */
    fastify.register(jwtPlugin);
    if (!fastify.config.API_PROD_MODE) {
      /** Configuracion de swagger */
      fastify.register(swaggerPlugin);
      /** Listado de rutas */
      fastify.register(routeSearcherPlugin);
    }
    /** Declaracion de esquemas globales */
    fastify.register(globalSchemasPlugin);
    /* Declaracion de rutas */
    fastify.register(autoloadPlugin, { path: __dirname });
    /* Incializacion del servidor */
    await fastify.ready();
    const basepath = await fastify.listen({
      port: fastify.config.API_PORT,
      host: fastify.config.API_HOST,
    });
    console.log(`Escuchando desde ${basepath}`);
  } catch (error) {
    fastify.log.error(error);
    process.exit(1);
  }
})();

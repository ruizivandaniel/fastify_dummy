const authorsOptions = require('./authors-options');
const authorsControllers = require('./authors-controllers');

async function authorsRoute(fastify, _, done) {

  fastify

    /** Obtener todos los libros. */
    .get(
      '/',
      authorsOptions.getAllAuthorsOptions,
      authorsControllers.makeGetAllAuthorsController(fastify)
    )

    /** Obtener el libro por id. */
    .get(
      '/:id',
      authorsOptions.getAuthorByIdOptions,
      authorsControllers.makeGetAuthorByIdController(fastify)
    )

    /** Crear nuevo author. */
    .post(
      '/',
      authorsOptions.createAuthorOptions,
      authorsControllers.makeCreateAuthorController(fastify)
    )

  done();
}

module.exports = authorsRoute;
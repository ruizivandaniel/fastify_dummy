/** Modelo de datos de obtener todos los autores. */
const getAllAuthorsOptions = {
  schema: {
    tags: ['Authors'],
    description: 'Retorna un array con todos los autores.',
    response: {
      200: {
        type: 'array',
        items: { $ref: 'authorSchema#' }
      }
    }
  }
};

/** Modelo de datos del obtener el autor por id. */
const getAuthorByIdOptions = {
  schema: {
    tags: ['Authors'],
    description: 'Retorna el autor perteneciente al id pasado por parametro.',
    params: {
      type: 'object',
      required: ['id'],
      properties: {
        id: { type: 'string' }
      },
    },
    response: {
      200: { $ref: 'authorSchema#' }
    }
  }
};

/** Modelo de datos de creacion de un author. */
const createAuthorOptions = {
  schema: {
    tags: ['Authors'],
    description: 'Crea un nuevo autor.',
    body: { $ref: 'authorSchema#' },
    response: {
      200: {
        message: { type: 'string' }
      }
    }
  }
};

module.exports = Object.freeze({
  getAllAuthorsOptions,
  createAuthorOptions,
  getAuthorByIdOptions,
});

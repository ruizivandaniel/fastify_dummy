/** Obtiene todos los autores que se encuentren en la base. */
const makeGetAllAuthorsController = (fastify) => {
  const collection = fastify.mongo.db.collection(fastify.config.AUTHORS_COLL);
  return async (req, res) => {
    try {
      const authors = await collection
        .find()
        .toArray();
      res.status(200).send(authors);
    } catch (error) {
      res.status(500).send({ message: 'Error interno al obtener autores.' });
    }
  };
};

/** Obtiene el autor por id. */
const makeGetAuthorByIdController = (fastify) => {
  const collection = fastify.mongo.db.collection(fastify.config.AUTHORS_COLL);
  return async (req, res) => {
    try {
      const id = fastify.mongo.ObjectId(req.params.id);
      const author = await collection.findOne({ _id: id });
      res.status(200).send(author);
    } catch (error) {
      res.status(500).send({ message: 'Error interno al obtener autores.' });
    }
  };
};

/** Crea un nuevo autor. */
const makeCreateAuthorController = (fastify) => {
  const collection = fastify.mongo.db.collection(fastify.config.AUTHORS_COLL);
  return async (req, res) => {
    try {
      const { firstName, lastName, age } = req.body;
      await collection.insertOne({ firstName, lastName, age });
      res.status(200).send({ message: 'Author creado satisfactoriamente.' });
    } catch (error) {
      res.status(500).send({ message: 'Error interno al crear autor.' });
    }
  };
};

module.exports = Object.freeze({
  makeGetAllAuthorsController,
  makeCreateAuthorController,
  makeGetAuthorByIdController,
});

const booksOptions = require('./books-options');
const booksControllers = require('./books-controllers');

async function booksRoute(fastify, _, done) {

  fastify

    /** Obtener todos los libros. */
    .get(
      '/',
      booksOptions.getAllBooksOptions,
      booksControllers.makeGetAllBooksController(fastify)
    )

    /** Obtener el libro por id. */
    .get(
      '/:id',
      booksOptions.getBookByIdOptions,
      booksControllers.makeGetBookByIdController(fastify)
    )

    /** Crear nuevo libro. */
    .post(
      '/',
      booksOptions.createBookOptions,
      booksControllers.makeCreateBookController(fastify)
    )

  // TODO: Crear findByName
  // TODO: Crear update
  // TODO: Crear delete
  done();
}

module.exports = booksRoute;
/** Obtiene todos los libros que se encuentren en la base. */
const makeGetAllBooksController = (fastify) => {
  const collection = fastify.mongo.db.collection(fastify.config.BOOKS_COLL);
  return async (req, res) => {
    try {
      // const books = [];
      // const cursor = await collection.find().toArray();
      // for await (const doc of cursor) {
      //   console.log(doc.author)
      //   const authorDoc = await authorsColl.findOne({ _id: doc.author });
      //   books.push({
      //     id: doc._id.toString(),
      //     title: doc.title,
      //     description: doc.description,
      //     author: {
      //       id: authorDoc._id.toString(),
      //       firstName: authorDoc.firstName,
      //       lastName: authorDoc.lastName,
      //       age: authorDoc.age,
      //     },
      //   });
      // }
      // res.status(200).send(books);
      const books = await collection.aggregate([
        {
          $lookup: {
            from: fastify.config.AUTHORS_COLL,
            localField: 'author',
            foreignField: '_id',
            as: 'author',
          }
        },
        {
          $project: {
            title: 1,
            description: 1,
            author: 1,
          }
        },
        {
          $unwind: "$author"
        }
      ]).toArray();
      res.send(books);
    } catch (error) {
      res.status(500).send({ message: 'Error interno' });
    }
  };
};

const makeGetBookByIdController = (fastify) => {
  const collection = fastify.mongo.db.collection(fastify.config.BOOKS_COLL);
  return async (req, res) => {
    try {
      const id = fastify.mongo.ObjectId(req.params.id);
      const book = await collection.findOne({ _id: id });
      res.status(200).send(book);
    } catch (error) {
      res.status(500).send({ message: 'Error interno' });
    }
  };
};

/** Crea un nuevo libro. */
const makeCreateBookController = (fastify) => {
  const collection = fastify.mongo.db.collection(fastify.config.BOOKS_COLL);
  return async (req, res) => {
    try {
      const { title, description, author: authorId } = req.body;
      await collection.insertOne({ title, description, author: fastify.mongo.ObjectId(authorId) });
      res.status(200).send({ message: 'Libro creado satisfactoriamente.' });
    } catch (error) {
      res.status(500).send({ message: 'Error interno al crear libro.' });
    }
  };
};

module.exports = Object.freeze({
  makeGetAllBooksController,
  makeGetBookByIdController,
  makeCreateBookController,
});

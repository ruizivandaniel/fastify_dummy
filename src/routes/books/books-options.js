/** Modelo de datos de obtener todos los libros. */
const getAllBooksOptions = {
  schema: {
    tags: ['Books'],
    description: 'Retorna un array con todos los libros.',
    response: {
      200: {
        type: 'array',
        items: { $ref: 'bookSchema#' },
      }
    }
  }
};

/** Modelo de datos de obtener libro por id. */
const getBookByIdOptions = {
  schema: {
    tags: ['Books'],
    description: 'Retorna el libro pertenenciente al id pasado por parametro.',
    params: {
      type: 'object',
      required: ['id'],
      properties: {
        id: { type: 'string' }
      },
    },
    response: {
      200: { $ref: 'bookSchema#' },
      // "4xx": { $ref: 'errorSchema#'}
    }
  }
};

/** Modelo de datos de creacion de un libro. */
const createBookOptions = {
  schema: {
    tags: ['Books'],
    description: 'Crea un nuevo libro.',
    body: {
      type: 'object',
      properties: {
        title: { type: 'string' },
        description: { type: 'string' },
        author: { type: 'string' },
      }
    },
    response: {
      200: {
        message: { type: 'string' }
      }
    }
  }
};

module.exports = Object.freeze({
  getAllBooksOptions,
  getBookByIdOptions,
  createBookOptions,
});

/** Modelo de un autor. */
module.exports = Object.freeze({
  $id: 'authorSchema',
  type: 'object',
  required: ['firstName', 'lastName', 'age'],
  properties: {
    _id: { $ref: 'ObjectId#' },
    firstName: {
      type: 'string',
      description: 'Nombre del autor.',
    },
    lastName: {
      type: 'string',
      description: 'Apellido del autor.',
    },
    age: {
      type: 'number',
      description: 'Edad del autor.',
    }
  }
});
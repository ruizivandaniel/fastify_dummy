/** Modelo de un libro. */
module.exports = Object.freeze({
  $id: 'bookSchema',
  type: 'object',
  required: ['title', 'description', 'author'],
  properties: {
    _id: { $ref: 'ObjectId#' },
    title: { type: 'string' },
    description: { type: 'string' },
    author: { $ref: 'authorSchema#' },
  }
});

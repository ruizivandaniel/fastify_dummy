/** Modelo de un usuario. */
module.exports = Object.freeze({
  $id: 'userSchema',
  type: 'object',
  properties: {
    user: {
      type: 'string',
      description: 'Nick del usuario.',
    },
    name: {
      type: 'string',
      description: 'Nombre del usuario.',
    },
    mail: {
      type: 'string',
      description: 'Email del usuario.',
    },
  },
});

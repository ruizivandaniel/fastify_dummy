/** Modelo de object id de mongo. */
module.exports = Object.freeze({
  $id: 'ObjectId',
  type: 'string',
  description: 'Identificador de mongo.',
});

# Pendientes a estudiar:

Ver cuales son los mejores plugins: https://www.fastify.io/ecosystem/#Core%20Plugins

Hooks: https://www.fastify.io/docs/latest/Reference/Hooks/
Routes: https://www.fastify.io/docs/latest/Reference/Routes/
Cors: https://github.com/fastify/fastify-cors
env: https://github.com/fastify/fastify-env
jwt: https://github.com/fastify/fastify-jwt



# Ideas:
[] Se podra hacer un tipo de objeto que sea "padre", ejemplo para manejar
la data que siempre debe responder backend.
[] Armar un schema para los errores (ver ej en backend mha).
[] Hacer un ejemplo de login con manejo de jwt.
[] Configurar CORS y ver ejemplos.
[] Hacer un schema de response.
[] Agregar casos de pruebas con jest o ver que da fastify.



# Recuerda que:
req.query -> te trae los queryparams (?value=20).
req.params -> te trae los params (/ruta/:id).